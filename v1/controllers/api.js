const _ = require("lodash");
const auth = require("../../common/authenticate");
const { ObjectId, Users } = require("../../models");

module.exports = {
    register,
    login,
    logout,
    sendOtp,
    verifyOtp,
    getProfile,
    updateProfile,
    changePassword,
    resetPassword,
    deleteAccount,
};

// OnBoarding APIs
async function register(req, res, next) {
    try {
        if (!req.body) throw new Error("MISSING_PARAMETERS");

        let dataObj = {
            username: req.body.username,
            password: req.body.password,
            fullName: req.body.fullName,
        };

        // trim keys
        dataObj = _.mapValues(dataObj, _.trim);
        // remove falsy
        dataObj = _.pickBy(dataObj, _.identity);

        const exists = await Users.findOne({ username: dataObj.username, isDeleted: false }).lean().exec();
        if (exists) throw new Error("USER_ALREADY_EXISTS");

        const doc = await Users.create(dataObj);
        await doc.setPassword(dataObj.password);
        await doc.save();

        return res.success("REGISTER_SUCCESSFULLY", doc.toJSON());
    } catch (error) {
        next(error);
    }
}

async function login(req, res, next) {
    try {
        if (!req.body) throw new Error("MISSING_PARAMETERS");

        let dataObj = {
            username: req.body.username,
            password: req.body.password,
        };

        // trim keys
        dataObj = _.mapValues(dataObj, _.trim);
        // remove falsy
        dataObj = _.pickBy(dataObj, _.identity);

        const doc = await Users.findOne({
            username: dataObj.username,
            isDeleted: false,
            isBlocked: false,
        });
        if (!doc) throw new Error("USER_NOT_FOUND");

        await doc.authenticate(dataObj.password);

        const session = await Sessions.create({
            userId: doc._id,
            ipAddress: req.ip,
            userAgent: req.headers["user-agent"],
            accessToken: authenticate.getToken({ _id: doc._id }),
        });

        const docObj = _.omit(doc.toJSON(), ["__v", "password"]);

        return res.success("LOGIN_SUCCESSFULLY", { ...docObj, accessToken: session.accessToken });
    } catch (error) {
        next(error);
    }
}

async function logout(req, res, next) {
    try {
        await Sessions.updateOne({ accessToken: req.user.session.accessToken }, { isDeleted: true }).exec();
        return res.success("LOGOUT_SUCCESSFULLY");
    } catch (error) {
        next(error);
    }
}

async function sendOtp(req, res, next) {
    try {
        if (!req.body) throw new Error("MISSING_PARAMETERS");

        let dataObj = {
            username: req.body.username,
        };

        // trim keys
        dataObj = _.mapValues(dataObj, _.trim);
        // remove falsy
        dataObj = _.pickBy(dataObj, _.identity);

        const userDoc = await Users.findOne({ username: dataObj.username });
        if (!userDoc) throw new Error("USER_NOT_FOUND");

        return res.success("OTP_SENT_SUCCESSFULLY");
    } catch (error) {
        next(error);
    }
}

async function verifyOtp(req, res, next) {
    try {
        if (!req.body) throw new Error("MISSING_PARAMETERS");

        let dataObj = {
            username: req.body.username,
            otpCode: req.body.otpCode,
        };

        // trim keys
        dataObj = _.mapValues(dataObj, _.trim);
        // remove falsy
        dataObj = _.pickBy(dataObj, _.identity);

        const userDoc = await Users.findOne({ username: dataObj.username });
        if (!userDoc) throw new Error("USER_NOT_FOUND");

        // const verify = await func.verifyOtpCode(value);
        const sentOTP = new Date().getDate().toString().padStart(2, "0") + (new Date().getMonth() + 1).toString().padStart(2, "0");

        if (dataObj.otpCode != sentOTP) throw new Error("INVALID_OTP");

        const session = await Sessions.create({
            userId: userDoc._id,
            ipAddress: req.ip,
            userAgent: req.headers["user-agent"],
            accessToken: authenticate.getToken({ _id: userDoc._id }),
        });

        const docObj = _.omit(userDoc.toJSON(), ["__v", "password"]);

        return res.success("OTP_VERIFIED", { ...docObj, accessToken: session.accessToken });
    } catch (error) {
        next(error);
    }
}

async function getProfile(req, res, next) {
    try {
        const doc = req.user.toJSON();

        return res.success("DATA_FETCHED", doc);
    } catch (error) {
        next(error);
    }
}

async function updateProfile(req, res, next) {
    try {
        if (!req.body) throw new Error("MISSING_PARAMETERS");

        let dataObj = {
            username: req.body.username,
            fullName: req.body.fullName,
        };

        // trim keys
        dataObj = _.mapValues(dataObj, _.trim);
        // remove falsy
        dataObj = _.pickBy(dataObj, _.identity);

        if (dataObj.username) {
            const check = await Users.findOne({ _id: { $nin: [req.user._id] }, username: dataObj.username }).exec();
            if (check) throw new Error("USERNAME_ALREADY_IN_USE");
        }

        const updated = await Users.updateOne({ _id: req.user._id }, { $set: dataObj }).exec();

        return res.success("UPDATED_SUCCESSFULLY", updated);
    } catch (error) {
        next(error);
    }
}

async function changePassword(req, res, next) {
    try {
        if (!req.body) throw new Error("MISSING_PARAMETERS");

        let dataObj = {
            oldPassword: req.body.oldPassword,
            newPassword: req.body.newPassword,
        };

        // trim keys
        dataObj = _.mapValues(dataObj, _.trim);
        // remove falsy
        dataObj = _.pickBy(dataObj, _.identity);

        const doc = await Users.findOne({ _id: req.user._id }).exec();
        await doc.authenticate(dataObj.oldPassword);
        await doc.setPassword(dataObj.newPassword);
        await doc.save();

        return res.success("PASSWORD_CHANGED");
    } catch (error) {
        next(error);
    }
}

async function resetPassword(req, res, next) {
    try {
        if (!req.body) throw new Error("MISSING_PARAMETERS");

        let dataObj = { newPassword: req.body.newPassword };

        // trim keys
        dataObj = _.mapValues(dataObj, _.trim);
        // remove falsy
        dataObj = _.pickBy(dataObj, _.identity);

        const doc = await Users.findOne({ _id: req.user._id }).exec();
        await doc.setPassword(dataObj.newPassword);
        await doc.save();

        return res.success("PASSWORD_RESET_SUCCESSFULLY");
    } catch (error) {
        next(error);
    }
}

async function deleteAccount(req, res, next) {
    try {
        if (!req.body) throw new Error("MISSING_PARAMETERS");

        let dataObj = { confirmation: req.body.confirmation };

        // trim keys
        dataObj = _.mapValues(dataObj, _.trim);
        // remove falsy
        dataObj = _.pickBy(dataObj, _.identity);

        if (dataObj.confirmation !== `DELETE ${req.user.username}`) throw new Error("INVALID_CONFIRMATION");

        await Users.updateOne({ _id: req.user._id }, { isDeleted: true }).exec();
        return res.success("ACCOUNT_DELETED");
    } catch (error) {
        next(error);
    }
}
